# Showcase for Addapptr

This app demonstrates various supported ad formats and their potential integration into an app.

The app features:

* A start-up interstitial
* Various banner formats presented in a feed-like user interface
* A sticky bottom banner use case
* Native ads, a more dynamic ad format, consisting of separate components, presented in a feed-like user interface
* Video ads, as a demonstration for a more engaging ad experience

---
## Installation
Clone this repository and import into **Android Studio**
```bash
git clone https://bitbucket.org/addapptr/showcase-android.git
```
---
## Configuration
### Keystores:
Create `/keystore.properties` with the following info:
```
storePassword=myStorePassword
keyPassword=mykeyPassword
keyAlias=myKeyAlias
storeFile=myStoreFileLocation
```

---
## Build variants
Use the Android Studio *Build Variants* button to choose between **release** and **debug** 

---
## Generating signed APK
### From Android Studio:
1. ***Build*** menu
2. ***Generate Signed APK...***
3. Fill in the keystore information *(you only need to do this once manually and then let Android Studio remember it)*
#### or 
1. Select **release** from *Build Variants*
2. ***Build*** menu
3. ***Build Bundle(s)/APK(s)***

### In terminal
```
sh
$ cd showcase
$ ./gradlew assembleRelease
```
---
## Maintainers
Current maintainers:

* [Damian Supera](https://bitbucket.org/damian_s/)
* [Michał Kapuściński](https://bitbucket.org/m_kapuscinski/)
