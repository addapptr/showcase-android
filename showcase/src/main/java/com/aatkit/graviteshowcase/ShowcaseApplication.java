package com.aatkit.graviteshowcase;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;

import com.aatkit.graviteshowcase.activity.StartActivity;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.AATKitConfiguration;
import com.intentsoftware.addapptr.AATKitRuntimeConfiguration;
import com.intentsoftware.addapptr.BannerCache;
import com.intentsoftware.addapptr.BannerCacheConfiguration;
import com.intentsoftware.addapptr.BannerConfiguration;
import com.intentsoftware.addapptr.BannerPlacementSize;
import com.intentsoftware.addapptr.FullscreenPlacement;
import com.intentsoftware.addapptr.InfeedBannerPlacement;
import com.intentsoftware.addapptr.ManagedConsent;
import com.intentsoftware.addapptr.NativeAdPlacement;
import com.intentsoftware.addapptr.RewardedVideoPlacement;
import com.intentsoftware.addapptr.StickyBannerPlacement;
import com.intentsoftware.addapptr.consent.CMP;
import com.intentsoftware.addapptr.consent.CMPGoogle;

import java.util.List;

public class ShowcaseApplication extends Application implements ManagedConsent.ManagedConsentDelegate, LifecycleObserver, AATKit.Delegate {

    private FullscreenPlacement fullscreenPlacement = null;
    private RewardedVideoPlacement rewardedVideoPlacement = null;
    private NativeAdPlacement nativeAdPlacement = null;

    private StickyBannerPlacement stickyBannerPlacement = null;
    private BannerCache bannerCache;
    private StickyBannerPlacement contentFeedBannerPlacement = null;
    private InfeedBannerPlacement inFeedBannerPlacement;

    private ManagedConsent managedConsent;
    private boolean cmpNeedsUI = false;

    private Activity activity;

    private static final String IAB_VENDOR_CONSENTS = "IABTCF_VendorConsents";
    private String vendorConsentsBinaryString;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public void onCreate() {
        super.onCreate();

        if (!isMainProcess()) {
            return;
        }

        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);

        vendorConsentsBinaryString = readBinaryStringFromSharedPrefs(this, IAB_VENDOR_CONSENTS);

        //AATKit
        AATKitConfiguration configuration = new AATKitConfiguration(this);

        configuration.setConsentRequired(true);

        AATKit.init(configuration);
        AATKit.enableDebugScreen();

        stickyBannerPlacement = AATKit.createStickyBannerPlacement("StickyBanner", BannerPlacementSize.Banner320x53);
        contentFeedBannerPlacement = AATKit.createStickyBannerPlacement("BannerInContentFeed", BannerPlacementSize.Banner320x53);

        BannerConfiguration bannerConfiguration = new BannerConfiguration();
        inFeedBannerPlacement = AATKit.createInfeedBannerPlacement("InFeedBanner", bannerConfiguration);

        BannerCacheConfiguration bannerCacheConfiguration = new BannerCacheConfiguration("BannerCache", 2);
        bannerCacheConfiguration.setShouldCacheAdditionalAdAtStart(true);
        bannerCache = AATKit.createBannerCache(bannerCacheConfiguration);

        fullscreenPlacement = AATKit.createFullscreenPlacement("Fullscreen");
        fullscreenPlacement.startAutoReload();
        rewardedVideoPlacement = AATKit.createRewardedVideoPlacement("RewardedVideo");
        nativeAdPlacement = AATKit.createNativeAdPlacement("NativeAd", true);
    }

    private boolean isMainProcess() {
        return getPackageName().equals(getCurrentProcessName());
    }

    private String getCurrentProcessName() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            return getProcessName();
        } else {
            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            if (manager != null) {
                int mypid = android.os.Process.myPid();
                List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = manager.getRunningAppProcesses();
                for (ActivityManager.RunningAppProcessInfo process : runningAppProcesses) {
                    if (process.pid == mypid) {
                        return process.processName;
                    }
                }
            }
            return null; // should never happen
        }
    }

    @Override
    public void aatkitObtainedAdRules(boolean fromTheServer) {

    }

    @Override
    public void aatkitUnknownBundleId() {

    }

    @Override
    public void managedConsentNeedsUserInterface(@NonNull ManagedConsent managedConsent) {
        cmpNeedsUI = true;
    }

    @Override
    public void managedConsentCMPFinished(@NonNull ManagedConsent.ManagedConsentState state) {
        cmpNeedsUI = false;
    }

    @Override
    public void managedConsentCMPFailedToLoad(@NonNull ManagedConsent managedConsent, String error) {
        cmpNeedsUI = false;
        if (this.managedConsent != null && activity != null) {
            this.managedConsent.reload(activity);
        }
    }

    @Override
    public void managedConsentCMPFailedToShow(@NonNull ManagedConsent managedConsent, String error) {
        cmpNeedsUI = true;
    }

    public boolean hasVendorConsentsBinaryString() {
        return vendorConsentsBinaryString != null;
    }

    public boolean isCmpNeedsUI() {
        return cmpNeedsUI;
    }

    public void showConsentDialogIfNeeded(final Activity activity) {
        if (managedConsent == null) {
            CMP cmp = new CMPGoogle(activity);
            managedConsent = new ManagedConsent(cmp, activity, this);
            AATKitRuntimeConfiguration newConf = new AATKitRuntimeConfiguration();
            newConf.setConsent(managedConsent);
            AATKit.reconfigure(newConf);
        } else if (cmpNeedsUI) {
            cmpNeedsUI = false;
            managedConsent.showIfNeeded(activity);
        }
    }

    public void showConsentDialog(final Activity activity) {
        if (managedConsent != null) {
            managedConsent.editConsent(activity);
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void onMoveToForeground() {
        if (activity != null) {
            Intent mainIntent = new Intent(this.getBaseContext(), StartActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_NO_ANIMATION
            );

            this.startActivity(mainIntent);
        }
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public StickyBannerPlacement getStickyBannerPlacement() {
        return stickyBannerPlacement;
    }

    public BannerCache getBannerCache() {
        return bannerCache;
    }

    public FullscreenPlacement getFullscreenPlacement() {
        return fullscreenPlacement;
    }

    public RewardedVideoPlacement getRewardedVideoPlacement() {
        return rewardedVideoPlacement;
    }

    public NativeAdPlacement getNativeAdPlacement() {
        return nativeAdPlacement;
    }

    public StickyBannerPlacement getContentFeedBannerPlacement() {
        return contentFeedBannerPlacement;
    }

    public InfeedBannerPlacement getInFeedBannerPlacement() {
        return inFeedBannerPlacement;
    }

    private String readBinaryStringFromSharedPrefs(Context context, String key) {
        String binaryString = PreferenceManager.getDefaultSharedPreferences(context).getString(key, null);
        if (binaryString == null) {
            Log.e(ShowcaseApplication.class.getName(), "Value for " + key + " not found in shared preferences");
        }

        return binaryString;
    }
}
