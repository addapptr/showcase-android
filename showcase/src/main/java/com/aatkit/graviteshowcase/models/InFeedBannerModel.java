package com.aatkit.graviteshowcase.models;

import android.util.Log;

import com.aatkit.graviteshowcase.listeners.AdLoadListener;
import com.intentsoftware.addapptr.BannerPlacementLayout;
import com.intentsoftware.addapptr.BannerRequest;
import com.intentsoftware.addapptr.BannerRequestCompletionListener;
import com.intentsoftware.addapptr.BannerRequestError;
import com.intentsoftware.addapptr.InfeedBannerPlacement;

import java.util.List;
import java.util.Map;

public class InFeedBannerModel implements BannerRequestCompletionListener {

    private BannerPlacementLayout bannerPlacementLayout = null;
    private BannerRequest bannerRequest;

    private AdLoadListener bannerLoadListener;
    private final int bannerPosition;
    private InfeedBannerPlacement inFeedBannerPlacement;
    private Map<String, List<String>> targetingInformation;

    public InFeedBannerModel(AdLoadListener bannerLoadListener, int bannerPosition, InfeedBannerPlacement inFeedBannerPlacement, Map<String, List<String>> targetingInformation) {
        this.bannerLoadListener = bannerLoadListener;
        this.bannerPosition = bannerPosition;
        this.inFeedBannerPlacement = inFeedBannerPlacement;
        this.targetingInformation = targetingInformation;
    }

    public BannerPlacementLayout getBannerPlacementLayout() {
        return bannerPlacementLayout;
    }

    public void reloadBanner() {
        if (bannerRequest == null) {
            bannerRequest = new BannerRequest(null);
            bannerRequest.setTargetingInformation(targetingInformation);
            inFeedBannerPlacement.requestAd(bannerRequest, this);
        }
    }

    public void onActivityDestroy() {
        bannerLoadListener = null;
        inFeedBannerPlacement = null;
        targetingInformation = null;
    }

    public boolean hasBanner() {
        return (bannerPlacementLayout != null);
    }

    public void removeAd() {

        if (bannerRequest != null) {
            inFeedBannerPlacement.cancel(bannerRequest); //cancel the ad loading
            bannerRequest = null;
        }
        if (bannerPlacementLayout != null) {
            bannerPlacementLayout.destroy(); //destroy the loaded banner
            bannerPlacementLayout = null;
        }
    }


    @Override
    public void onRequestCompleted(BannerPlacementLayout layout, BannerRequestError error) {
        if (layout != null) {
            bannerPlacementLayout = layout;
            bannerLoadListener.onRequestCompleted(bannerPosition);
            bannerRequest = null;
        } else {
            Log.w("InFeedBanner", "Failed to load banner, reason: " + error.getMessage());
        }
    }
}
