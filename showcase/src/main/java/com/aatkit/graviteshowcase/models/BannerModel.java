package com.aatkit.graviteshowcase.models;

import android.view.View;
import android.view.ViewGroup;

import com.aatkit.graviteshowcase.listeners.AdLoadListener;
import com.intentsoftware.addapptr.StickyBannerPlacement;

public class BannerModel {

    private View bannerPlacementLayout;

    private final AdLoadListener bannerLoadListener;
    private final int bannerPosition;
    private final StickyBannerPlacement bannerPlacement;

    public BannerModel(AdLoadListener bannerLoadListener, int bannerPosition, StickyBannerPlacement bannerPlacement) {
        this.bannerLoadListener = bannerLoadListener;
        this.bannerPosition = bannerPosition;
        this.bannerPlacement = bannerPlacement;
    }

    public View getBannerPlacementLayout() {
        return bannerPlacementLayout;
    }

    public void reloadBanner() {
        bannerPlacement.reload(true);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean hasBanner() {
        return (bannerPlacementLayout != null);
    }

    public void removeAd() {

        if (bannerPlacementLayout != null) {

            if (bannerPlacementLayout.getParent() != null) {
                ViewGroup parent = (ViewGroup) bannerPlacementLayout.getParent();
                parent.removeView(bannerPlacementLayout);
            }
            bannerPlacementLayout = null;
        }
    }

    public void setBannerPlacementLayout(View bannerPlacementLayout) {

        if (this.bannerPlacementLayout != null) {
            this.bannerPlacementLayout = null;
        }
        this.bannerPlacementLayout = bannerPlacementLayout;
        bannerLoadListener.onRequestCompleted(bannerPosition);
    }
}
