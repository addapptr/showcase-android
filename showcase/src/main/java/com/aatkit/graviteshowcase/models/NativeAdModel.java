package com.aatkit.graviteshowcase.models;

import com.aatkit.graviteshowcase.listeners.AdLoadListener;
import com.intentsoftware.addapptr.NativeAdPlacement;
import com.intentsoftware.addapptr.ad.NativeAdData;

public class NativeAdModel {

    private AdLoadListener adLoadListener;
    private NativeAdData nativeAdData;

    private final NativeAdPlacement nativeAdPlacement;
    private final int adPosition;
    private boolean reloadAd = true;
    private boolean reportedAdSpace;

    public NativeAdModel(AdLoadListener adLoadListener, int adPosition, NativeAdPlacement nativeAdPlacement) {

        this.adLoadListener = adLoadListener;
        this.adPosition = adPosition;

        this.nativeAdPlacement = nativeAdPlacement;
    }

    public boolean hasNativeAd() {
        return (nativeAdData != null);
    }

    public void reportAdSpace() {
        if (!reportedAdSpace) { //report adspace only once for one ad
            reportedAdSpace = true;
            nativeAdPlacement.countAdSpace();
        }
    }

    public NativeAdData getNativeAdData() {
        return nativeAdData;
    }

    public void setNativeAdData(NativeAdData nativeAdData) {
        this.nativeAdData = nativeAdData;

        if (adLoadListener != null)
            adLoadListener.onRequestCompleted(adPosition);
    }

    public void reloadAd() {
        if (!nativeAdPlacement.isFrequencyCapReached()) {
            if (reloadAd) {
                reloadAd = false;
                nativeAdPlacement.reload();
            }
        }
    }

    public void removeAd() {

        if (nativeAdData != null)
            nativeAdData.detachFromLayout();

        adLoadListener = null;
        nativeAdData = null;
        reloadAd = false;
        reportedAdSpace = false;
    }
}