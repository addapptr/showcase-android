package com.aatkit.graviteshowcase.models;

public class DescriptionModel {

    private final String description;

    public DescriptionModel(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
