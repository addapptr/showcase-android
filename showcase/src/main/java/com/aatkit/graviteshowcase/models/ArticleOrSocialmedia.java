package com.aatkit.graviteshowcase.models;

public class ArticleOrSocialmedia {

    private final String title;
    private final String description;
    private final int mainImageResources;
    private final int iconResources;
    private int numberOfHearts;
    private int numberOfComments;
    private boolean isCheckToggleButton = false;

    public ArticleOrSocialmedia(String title, String description, int mainImageResources, int iconResources, int numberOfHearts, int numberOfComments) {
        this.title = title;
        this.description = description;
        this.mainImageResources = mainImageResources;

        this.iconResources = iconResources;
        this.numberOfHearts = numberOfHearts;
        this.numberOfComments = numberOfComments;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getMainImageResources() {
        return mainImageResources;
    }

    public int getIconResources() {
        return iconResources;
    }

    public int getNumberOfHearts() {
        return numberOfHearts;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public boolean isCheckToggleButton() {
        return isCheckToggleButton;
    }

    public void incrementNumberOfComment() {
        this.numberOfComments++;
    }

    public void incrementNumberOfHearts() {
        this.numberOfHearts++;
        this.isCheckToggleButton = true;
    }

    public void decrementNumberOfHearts() {
        this.numberOfHearts--;
        this.isCheckToggleButton = false;
    }
}
