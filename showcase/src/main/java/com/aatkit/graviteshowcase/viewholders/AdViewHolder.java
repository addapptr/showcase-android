package com.aatkit.graviteshowcase.viewholders;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aatkit.graviteshowcase.R;
import com.applovin.mediation.nativeAds.MaxNativeAdView;
import com.applovin.mediation.nativeAds.MaxNativeAdViewBinder;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.intentsoftware.addapptr.AdNetwork;
import com.intentsoftware.addapptr.ad.NativeAdData;

public class AdViewHolder extends RecyclerView.ViewHolder {

    public ViewGroup nativeAdView;

    public final FrameLayout adFrame;

    public MediaView mediaView;
    public View mainImageView = null;

    public TextView titleView;
    public View iconView;
    public TextView descriptionView;
    public Button ctaView;

    public AdViewHolder(View itemView) {
        super(itemView);
        adFrame = itemView.findViewById(R.id.empty_ad_view);
    }

    @SuppressLint("InflateParams")
    public void prepareViewForNativeAd(NativeAdData nativeAd, Activity activity) {

        if (nativeAd != null) {
            adFrame.removeAllViews();
            if (nativeAd.getNetwork() == AdNetwork.ADMOB || nativeAd.getNetwork() == AdNetwork.DFP) {
                nativeAdView = (NativeAdView) activity.getLayoutInflater().inflate(R.layout.layout_for_admob_native_unified_ad, null);
                mediaView = nativeAdView.findViewById(R.id.media_view);

                titleView = nativeAdView.findViewById(R.id.title_view);
                iconView = nativeAdView.findViewById(R.id.icon_view);
                descriptionView = nativeAdView.findViewById(R.id.description_view);
                ctaView = nativeAdView.findViewById(R.id.CTA_view);
            } else if (nativeAd.getNetwork() == AdNetwork.FACEBOOK) {
                nativeAdView = (ViewGroup) activity.getLayoutInflater().inflate(R.layout.layout_for_facebook_native_ad, null);
                mainImageView = nativeAdView.findViewById(R.id.main_image_view);

                titleView = nativeAdView.findViewById(R.id.title_view);
                iconView = nativeAdView.findViewById(R.id.icon_view);
                descriptionView = nativeAdView.findViewById(R.id.description_view);
                ctaView = nativeAdView.findViewById(R.id.CTA_view);
            } else if (nativeAd.getNetwork() == AdNetwork.APPLOVINMAX) {
                MaxNativeAdViewBinder maxNativeAdViewBinder = new MaxNativeAdViewBinder.Builder(R.layout.layout_for_applovinmax_native_ad)
                        .setTitleTextViewId(R.id.title_text_view)
                        .setBodyTextViewId(R.id.body_text_view)
                        .setAdvertiserTextViewId(R.id.advertiser_textView)
                        .setIconImageViewId(R.id.icon_image_view)
                        .setMediaContentViewGroupId(R.id.media_view_container)
                        .setOptionsContentViewGroupId(R.id.ad_options_view)
                        .setCallToActionButtonId(R.id.cta_button)
                        .build();
                nativeAdView = new MaxNativeAdView(maxNativeAdViewBinder, activity);
            } else {
                nativeAdView = (ViewGroup) activity.getLayoutInflater().inflate(R.layout.layout_for_native_normal_ad, null);
                mainImageView = nativeAdView.findViewById(R.id.main_image_view);

                titleView = nativeAdView.findViewById(R.id.title_view);
                iconView = nativeAdView.findViewById(R.id.icon_view);
                descriptionView = nativeAdView.findViewById(R.id.description_view);
                ctaView = nativeAdView.findViewById(R.id.CTA_view);
            }

            ViewGroup.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
            adFrame.addView(nativeAdView, params);
        }
    }
}