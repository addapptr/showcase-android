package com.aatkit.graviteshowcase.viewholders;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.adapters.NativeAdViewAdapter;

public class ArticleOrSocialmediaViewHolder extends RecyclerView.ViewHolder {

    public final TextView titleView;
    public final ImageView mainImageView;
    public final TextView articleDescriptionView;
    public final ImageView socialMediaIconView;
    public final TextView socialMediaNumberOfHearts;
    public final TextView socialMediaNumberOfComments;
    private final EditText socialMediaEnterComment;

    public final ToggleButton heart;

    public final LinearLayout layoutBottomSocialMedia;

    private final NativeAdViewAdapter.HolderEventListener holderEventListener;
    private int position;

    public ArticleOrSocialmediaViewHolder(View itemView, final NativeAdViewAdapter.HolderEventListener holderEventListener) {
        super(itemView);

        this.holderEventListener = holderEventListener;

        articleDescriptionView = itemView.findViewById(R.id.article_description_view);

        titleView = itemView.findViewById(R.id.title_view);
        mainImageView = itemView.findViewById(R.id.main_image_view);
        socialMediaIconView = itemView.findViewById(R.id.icon_view);
        socialMediaNumberOfHearts = itemView.findViewById(R.id.number_of_heart);
        socialMediaNumberOfComments = itemView.findViewById(R.id.number_of_comment);
        socialMediaEnterComment = itemView.findViewById(R.id.enter_comment);

        heart = itemView.findViewById(R.id.heart);
        heart.setOnClickListener(createOnClickListener());

        layoutBottomSocialMedia = itemView.findViewById(R.id.layout_bottom_social_media);

        socialMediaEnterComment.setOnEditorActionListener(createOnEditorActionListener());
    }

    private View.OnClickListener createOnClickListener() {
        return view -> {
            if (heart.isChecked()) {
                holderEventListener.onWhiteHeartClick(position);
                heart.setChecked(false);
            } else {
                holderEventListener.onOrangeHeartClick(position);
                heart.setChecked(true);
            }
        };
    }

    private TextView.OnEditorActionListener createOnEditorActionListener() {
        return (textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE) {
                if (!socialMediaEnterComment.getText().toString().equals("")) {
                    holderEventListener.onEditTextClickDone(position);
                    InputMethodManager imm = (InputMethodManager) textView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                    }
                    socialMediaEnterComment.setText(null);
                    return true;
                }
            }
            return false;
        };
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
