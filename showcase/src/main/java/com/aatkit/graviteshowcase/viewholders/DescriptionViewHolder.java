package com.aatkit.graviteshowcase.viewholders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aatkit.graviteshowcase.R;

public class DescriptionViewHolder extends RecyclerView.ViewHolder {

    public final TextView descriptionView;

    public DescriptionViewHolder(View itemView) {
        super(itemView);
        descriptionView = itemView.findViewById(R.id.description);
    }
}
