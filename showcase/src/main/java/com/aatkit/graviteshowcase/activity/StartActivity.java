package com.aatkit.graviteshowcase.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.ShowcaseApplication;
import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.FullscreenPlacementListener;
import com.intentsoftware.addapptr.Placement;

public class StartActivity extends Activity {

    private static final String EXTRA_AD_SHOWN_FROM_START = "adShownFromStart";
    private boolean adShownFromStart = false;

    private static final int SPLASH_TIME = 1000;
    private static final int INTERSTITIAL_LOAD_TIME = 5000;
    private static final int SECOND = 1000;

    private CountDownTimer countDownTimer;
    private TextView timerTextView;
    private long timeForLoadingScreen;

    private boolean isLoadingScreen = false;

    private LinearLayout layoutWithTimer;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        timerTextView = findViewById(R.id.timer_view);
        timerTextView.setText((SPLASH_TIME / SECOND) + " seconds");

        layoutWithTimer = findViewById(R.id.layout_with_timer);

        onResumeTimer(SPLASH_TIME);
        countDownTimer.start();
    }


    @Override
    protected void onResume() {
        super.onResume();
        AATKit.onActivityResume(this);
        ((ShowcaseApplication) getApplication()).showConsentDialogIfNeeded(this);
    }

    @Override
    protected void onPause() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        AATKit.onActivityPause(this);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        ((ShowcaseApplication) getApplication()).getFullscreenPlacement().setListener(null);

        if (countDownTimer != null)
            countDownTimer.cancel();
        countDownTimer = null;
        timeForLoadingScreen = 0;

        timerTextView = null;
        adShownFromStart = false;

        super.onDestroy();
    }

    private void onResumeTimer(final long millisInFuture) {
        countDownTimer = new CountDownTimer(millisInFuture, SECOND) {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTick(long millisUntilFinished) {

                if (isLoadingScreen) {
                    String secondText;
                    if ((millisUntilFinished + SECOND) / SECOND <= 1)
                        secondText = " second";
                    else
                        secondText = " seconds";

                    timeForLoadingScreen = millisUntilFinished;

                    timerTextView.setText((millisUntilFinished + SECOND) / SECOND + secondText);
                }
            }

            @Override
            public void onFinish() {

                if (isLoadingScreen || !((ShowcaseApplication) getApplication()).hasVendorConsentsBinaryString())
                    startMainActivity();
                else {
                    isLoadingScreen = true;

                    ((ShowcaseApplication) getApplication()).getFullscreenPlacement().setListener(createFullscreenPlacementListener());

                    boolean isFrequencyCapReachedForPlacement = ((ShowcaseApplication) getApplication()).getFullscreenPlacement().isFrequencyCapReached();

                    onResumeTimer(INTERSTITIAL_LOAD_TIME);

                    if (isFrequencyCapReachedForPlacement) { // if impression cap is reached
                        if (countDownTimer != null) {
                            countDownTimer.onFinish();
                            return;
                        }
                    }

                    if (((ShowcaseApplication) getApplication()).getFullscreenPlacement().hasAd()) {
                        showFullscreen();
                        return;
                    }

                    layoutWithTimer.setVisibility(View.VISIBLE);
                    countDownTimer.start();
                }
            }
        };
    }

    // Run when close consent dialog
    private Runnable createOnCompletion() {
        return this::startMainActivity;
    }

    private void startMainActivity() {
        countDownTimer.cancel();
        Intent mainIntent = new Intent(StartActivity.this, MainActivity.class);
        mainIntent.putExtra(EXTRA_AD_SHOWN_FROM_START, adShownFromStart);
        StartActivity.this.startActivity(mainIntent);
        StartActivity.this.finish();
    }

    private void showFullscreen() {
        if (countDownTimer != null)
            countDownTimer.cancel();
        if (!adShownFromStart) {
            adShownFromStart = ((ShowcaseApplication) getApplication()).getFullscreenPlacement().show();
            if (!adShownFromStart) {
                onResumeTimer(timeForLoadingScreen);
                countDownTimer.start();
            }
        }
    }

    private FullscreenPlacementListener createFullscreenPlacementListener() {
        return new FullscreenPlacementListener() {

            @Override
            public void onNoAd(@NonNull Placement placement) {

            }

            @Override
            public void onHaveAd(@NonNull Placement placement) {
                showFullscreen();

            }

            @Override
            public void onResumeAfterAd(@NonNull Placement placement) {
                if (countDownTimer != null)
                    countDownTimer.onFinish();
            }

            @Override
            public void onPauseForAd(@NonNull Placement placement) {

            }
        };
    }


    //    private void showConsentDialogIfNeeded() {
    //        if (isLoadingScreen) {
    //            if (!((ShowcaseApplication) getApplication()).showConsentDialogIfNeeded(this, createOnCompletion())) {
    //
    //                showcaseApplication.setListener(createOnAATKitEventListener());
    //
    //                boolean isFrequencyCapReachedForPlacement = AATKit.isFrequencyCapReachedForPlacement(showcaseApplication.getFullscreenPlacementId());
    //
    //                onResumeTimer(INTERSTITIAL_LOAD_TIME);
    //
    //                if (isFrequencyCapReachedForPlacement) { // if impression cap is reached
    //                    if (countDownTimer != null) {
    //                        countDownTimer.onFinish();
    //                        return;
    //                    }
    //                }
    //
    //                if (AATKit.hasAdForPlacement(showcaseApplication.getFullscreenPlacementId())) {
    //                    showFullscreen();
    //                    return;
    //                }
    //
    //                layoutWithTimer.setVisibility(View.VISIBLE);
    //                countDownTimer.start();
    //            }
    //        }
    //    }
}
