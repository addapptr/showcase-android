package com.aatkit.graviteshowcase.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.ShowcaseApplication;
import com.aatkit.graviteshowcase.fragments.FragmentBannerCache;
import com.aatkit.graviteshowcase.fragments.FragmentBannerContentFeed;
import com.aatkit.graviteshowcase.fragments.FragmentBannerSticky;
import com.aatkit.graviteshowcase.fragments.FragmentInterstitial;
import com.aatkit.graviteshowcase.fragments.FragmentNativeAd;
import com.aatkit.graviteshowcase.fragments.FragmentRewardedVideo;
import com.google.android.material.navigation.NavigationView;
import com.intentsoftware.addapptr.AATKit;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static boolean fullscreenShown = false;

    private Fragment fragment;
    private NavigationView navigationView;
    private ActionBarDrawerToggle toggle;

    private TextView textViewAddapptrWebPage;
    private ImageView imageViewAddapptrLogo;

    private VideoView videoView;
    private DrawerLayout drawer;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ((ShowcaseApplication) getApplication()).setActivity(this);

        if (getActionBar() != null)
            getActionBar().setHomeButtonEnabled(false);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View linearLayout = navigationView.getHeaderView(0);

        textViewAddapptrWebPage = linearLayout.findViewById(R.id.tv_addapptr_web_page);
        textViewAddapptrWebPage.setOnTouchListener(createOnTouchListener());

        imageViewAddapptrLogo = linearLayout.findViewById(R.id.iv_addapptr_logo);
        imageViewAddapptrLogo.setOnTouchListener(createOnTouchListener());

        videoView = linearLayout.findViewById(R.id.video_view);
        videoView.setVideoPath("android.resource://" + getPackageName() + "/raw/" + R.raw.bubble_animation_solarsystem);
        videoView.setOnErrorListener((mp, what, extra) -> {
            videoView.setVisibility(View.GONE);
            return true;
        });
        videoView.start();

        fragment = new FragmentInterstitial();
        fragment.setArguments(getIntent().getExtras());

        navigationView.setCheckedItem(R.id.nav_interstitial);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AATKit.onActivityResume(this);

        if (fullscreenShown) {
            fullscreenShown = false;
        }

        if (((ShowcaseApplication) getApplication()).isCmpNeedsUI()) {
            ((ShowcaseApplication) getApplication()).showConsentDialogIfNeeded(this);
        }


        onReplaceFragment(fragment);
    }

    @Override
    protected void onPause() {
        AATKit.onActivityPause(this);
        super.onPause();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onDestroy() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.remove(fragment);
        ft.commitAllowingStateLoss();

        fragment = null;
        fullscreenShown = false;
        drawer.removeDrawerListener(toggle);

        navigationView.setNavigationItemSelectedListener(null);
        navigationView = null;

        textViewAddapptrWebPage.setOnTouchListener(null);
        textViewAddapptrWebPage = null;
        imageViewAddapptrLogo.setOnTouchListener(null);
        imageViewAddapptrLogo = null;

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id != R.id.privacy_settings) {
            if (((ShowcaseApplication) getApplication()).isCmpNeedsUI()) {
                ((ShowcaseApplication) getApplication()).showConsentDialogIfNeeded(this);
            } else {
                fullscreenShown = ((ShowcaseApplication) getApplication()).getFullscreenPlacement().show();
            }
        }

        if (id == R.id.nav_rewardedvideo) {
            fragment = new FragmentRewardedVideo();
        } else if (id == R.id.nav_native) {
            fragment = new FragmentNativeAd();
        } else if (id == R.id.nav_banner_sticky) {
            fragment = new FragmentBannerSticky();
        } else if (id == R.id.nav_banner_contentfeed) {
            fragment = new FragmentBannerContentFeed();
        } else if (id == R.id.nav_banner_cache) {
            fragment = new FragmentBannerCache();
        } else if (id == R.id.privacy_settings) {
            ((ShowcaseApplication) getApplication()).showConsentDialog(this);
        } else {
            fragment = new FragmentInterstitial();
        }

        if (!fullscreenShown) {
            onReplaceFragment(fragment);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onReplaceFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                fragmentManager.popBackStack();
            }

            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.mainFrame, fragment);
            ft.commit();
        }
    }

    private View.OnTouchListener createOnTouchListener() {
        return (view, motionEvent) -> {

            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                view.setBackgroundColor(getResources().getColor(R.color.gradientEndColor));
                return true;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                view.setBackgroundColor(0);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("https://gravite.net/"));
                startActivity(intent);
                view.performClick();
                return true;
            } else if (motionEvent.getAction() == MotionEvent.ACTION_CANCEL) {
                view.setBackgroundColor(0);
                return true;
            }

            return false;
        };
    }
}
