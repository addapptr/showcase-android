package com.aatkit.graviteshowcase.adapters;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.models.BannerCacheModel;
import com.aatkit.graviteshowcase.models.BannerModel;
import com.aatkit.graviteshowcase.models.InFeedBannerModel;
import com.aatkit.graviteshowcase.models.NativeAdModel;
import com.aatkit.graviteshowcase.viewholders.AdViewHolder;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.intentsoftware.addapptr.AdNetwork;
import com.intentsoftware.addapptr.BannerPlacementLayout;
import com.intentsoftware.addapptr.internal.module.Utils;

abstract class AdapterWithAds extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    boolean isArticle = true;

    final LinearLayout.LayoutParams paramForVisibleView = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    final LinearLayout.LayoutParams paramForInvisibleView = new LinearLayout.LayoutParams(0, 0, 0);
    final LinearLayout.LayoutParams paramForVisibleViewWithWeight = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1);

    @NonNull
    RecyclerView.ViewHolder getBannerViewHolder(ViewGroup parent) {
        View adView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_empty, parent, false);
        return new AdViewHolder(adView);
    }

    void bindAdViewHolder(RecyclerView.ViewHolder holder, Object adObject, Activity activity) {

        AdViewHolder adViewHolder = (AdViewHolder) holder;

        if (adObject instanceof InFeedBannerModel) {
            adViewHolder.adFrame.removeAllViews();

            InFeedBannerModel inFeedBannerModel = (InFeedBannerModel) adObject;

            BannerPlacementLayout bannerPlacementLayout = inFeedBannerModel.getBannerPlacementLayout();
            if (bannerPlacementLayout != null) {
                if (bannerPlacementLayout.getParent() != null) {
                    ((FrameLayout) bannerPlacementLayout.getParent()).removeAllViews();
                }
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
                adViewHolder.adFrame.addView(bannerPlacementLayout, params);
            }
        } else if (adObject instanceof BannerModel) {
            adViewHolder.adFrame.removeAllViews();

            BannerModel bannerModel = (BannerModel) adObject;

            View bannerPlacementLayout = bannerModel.getBannerPlacementLayout();
            if (bannerPlacementLayout != null) {
                if (bannerPlacementLayout.getParent() != null) {
                    ((FrameLayout) bannerPlacementLayout.getParent()).removeAllViews();
                }
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);
                adViewHolder.adFrame.addView(bannerPlacementLayout, params);
            }
        } else if (adObject instanceof BannerCacheModel) {

            adViewHolder.adFrame.removeAllViews();
            BannerCacheModel bannerCacheModel = (BannerCacheModel) adObject;
            BannerPlacementLayout bannerPlacementLayout = bannerCacheModel.getBannerPlacementLayout();
            if (bannerPlacementLayout != null) {
                if (bannerPlacementLayout.getParent() != null) {
                    ((FrameLayout) bannerPlacementLayout.getParent()).removeAllViews();
                }
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER);

                adViewHolder.adFrame.addView(bannerPlacementLayout, params);
            }
        } else if (adObject instanceof NativeAdModel) {

            NativeAdModel nativeAdModel = (NativeAdModel) adObject;
            nativeAdModel.reportAdSpace();

            adViewHolder.prepareViewForNativeAd(nativeAdModel.getNativeAdData(), activity);

            if (nativeAdModel.hasNativeAd()) {

                if (nativeAdModel.getNativeAdData().getNetwork() == AdNetwork.ADMOB || nativeAdModel.getNativeAdData().getNetwork() == AdNetwork.DFP || nativeAdModel.getNativeAdData().getNetwork() == AdNetwork.DFPDIRECT) {

                    ((NativeAdView) adViewHolder.nativeAdView).setCallToActionView(adViewHolder.ctaView);
                    ((NativeAdView) adViewHolder.nativeAdView).setBodyView(adViewHolder.descriptionView);
                    ((NativeAdView) adViewHolder.nativeAdView).setHeadlineView(adViewHolder.titleView);
                    ((NativeAdView) adViewHolder.nativeAdView).setIconView(adViewHolder.iconView);
                    ((NativeAdView) adViewHolder.nativeAdView).setMediaView(adViewHolder.mediaView);
                }

                if (adViewHolder.mainImageView instanceof ImageView)
                    Utils.loadBitmapForView(nativeAdModel.getNativeAdData().getImageUrl(), (ImageView) adViewHolder.mainImageView);
                if (adViewHolder.iconView instanceof ImageView)
                    Utils.loadBitmapForView(nativeAdModel.getNativeAdData().getIconUrl(), (ImageView) adViewHolder.iconView);

                if (adViewHolder.titleView != null)
                    adViewHolder.titleView.setText(nativeAdModel.getNativeAdData().getTitle());
                if (adViewHolder.descriptionView != null)
                    adViewHolder.descriptionView.setText(nativeAdModel.getNativeAdData().getDescription());
                if (adViewHolder.ctaView != null)
                    adViewHolder.ctaView.setText(nativeAdModel.getNativeAdData().getCallToAction());

                View sponsoredImage = nativeAdModel.getNativeAdData().getBrandingLogo();
                FrameLayout sponsoredImageFL = adViewHolder.nativeAdView.findViewById(R.id.sponsored_image);
                if (sponsoredImage != null) {
                    if (sponsoredImage.getParent() != null) {
                        ((FrameLayout) sponsoredImage.getParent()).removeAllViews();
                    }
                    sponsoredImageFL.addView(sponsoredImage);
                }

                nativeAdModel.getNativeAdData().attachToLayout(adViewHolder.nativeAdView, adViewHolder.mainImageView, adViewHolder.iconView, adViewHolder.ctaView);

                if (isArticle) {
                    if (adViewHolder.iconView != null)
                        adViewHolder.iconView.setLayoutParams(paramForInvisibleView);
                    if (adViewHolder.descriptionView != null)
                        adViewHolder.descriptionView.setLayoutParams(paramForVisibleViewWithWeight);
                } else {
                    if (adViewHolder.iconView != null)
                        adViewHolder.iconView.setLayoutParams(paramForVisibleView);
                    if (adViewHolder.descriptionView != null)
                        adViewHolder.descriptionView.setLayoutParams(paramForInvisibleView);
                }
            }
        }
    }


    public void showArticle(boolean article) {
        isArticle = article;
    }
}
