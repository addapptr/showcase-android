package com.aatkit.graviteshowcase.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.models.DescriptionModel;
import com.aatkit.graviteshowcase.models.NewsModel;
import com.aatkit.graviteshowcase.viewholders.DescriptionViewHolder;
import com.aatkit.graviteshowcase.viewholders.NewsViewHolder;

import java.util.List;

public class NewsAdapter extends AdapterWithAds {

    private final List<Object> newsList;
    private static final int TYPE_NEWS = 1;
    private static final int TYPE_DESCRIPTION = 2;
    private static final int TYPE_BANNER = 3;

    public NewsAdapter(@NonNull List<Object> newsList) {
        this.newsList = newsList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case 1:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_news, parent, false);
                return new NewsViewHolder(itemView);
            case 2:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_description, parent, false);
                return new DescriptionViewHolder(itemView);
            case 3:
                return getBannerViewHolder(parent);
            default:
                return new RecyclerView.ViewHolder(parent) {
                };
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (newsList.get(position) instanceof NewsModel) {
            NewsModel newsModel = (NewsModel) newsList.get(position);
            NewsViewHolder newsViewHolder = (NewsViewHolder) holder;
            newsViewHolder.titleView.setText(newsModel.getTitle());
            newsViewHolder.newsView.setText(newsModel.getNews());
        } else if (newsList.get(position) instanceof DescriptionModel) {
            DescriptionModel descriptionModel = (DescriptionModel) newsList.get(position);
            DescriptionViewHolder descriptionViewHolder = (DescriptionViewHolder) holder;
            descriptionViewHolder.descriptionView.setText(descriptionModel.getDescription());
        } else {
            bindAdViewHolder(holder, newsList.get(position), null);
        }
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (newsList.get(position) instanceof NewsModel) {
            return TYPE_NEWS;
        } else if (newsList.get(position) instanceof DescriptionModel) {
            return TYPE_DESCRIPTION;
        } else {
            return TYPE_BANNER;
        }
    }
}
