package com.aatkit.graviteshowcase.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.models.ArticleOrSocialmedia;
import com.aatkit.graviteshowcase.models.DescriptionModel;
import com.aatkit.graviteshowcase.models.NativeAdModel;
import com.aatkit.graviteshowcase.viewholders.ArticleOrSocialmediaViewHolder;
import com.aatkit.graviteshowcase.viewholders.DescriptionViewHolder;

import java.util.List;

public class NativeAdViewAdapter extends AdapterWithAds {

    private final List<Object> articleOrSocialMediaList;
    private static final int TYPE_ARTICLE_OR_SOCIALMEDIA = 1;
    private static final int TYPE_TEXT = 2;
    private static final int TYPE_NATIVE_AD = 3;

    private final Activity activity;

    public interface HolderEventListener {
        void onEditTextClickDone(int position);

        void onWhiteHeartClick(int position);

        void onOrangeHeartClick(int position);
    }

    public NativeAdViewAdapter(List<Object> articleOrSocialMediaList, Activity activity) {
        this.articleOrSocialMediaList = articleOrSocialMediaList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case TYPE_ARTICLE_OR_SOCIALMEDIA:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_article_or_socialmedia, parent, false);
                return new ArticleOrSocialmediaViewHolder(itemView, createOnHolderEventListener());
            case TYPE_TEXT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_description, parent, false);
                return new DescriptionViewHolder(itemView);
            case TYPE_NATIVE_AD:
                return getBannerViewHolder(parent);
            default:
                return new RecyclerView.ViewHolder(parent) {
                };
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (articleOrSocialMediaList.get(position) instanceof ArticleOrSocialmedia) {
            ArticleOrSocialmedia aArticleOrSocialmedia = (ArticleOrSocialmedia) articleOrSocialMediaList.get(position);
            ArticleOrSocialmediaViewHolder articleOrSocialmediaViewHolder = (ArticleOrSocialmediaViewHolder) holder;

            articleOrSocialmediaViewHolder.articleDescriptionView.setText(aArticleOrSocialmedia.getDescription());
            articleOrSocialmediaViewHolder.titleView.setText(aArticleOrSocialmedia.getTitle());
            articleOrSocialmediaViewHolder.mainImageView.setImageResource(aArticleOrSocialmedia.getMainImageResources());
            articleOrSocialmediaViewHolder.socialMediaIconView.setImageResource(aArticleOrSocialmedia.getIconResources());
            articleOrSocialmediaViewHolder.socialMediaNumberOfComments.setText(String.valueOf(aArticleOrSocialmedia.getNumberOfComments()));
            articleOrSocialmediaViewHolder.socialMediaNumberOfHearts.setText(String.valueOf(aArticleOrSocialmedia.getNumberOfHearts()));

            if (articleOrSocialmediaViewHolder.heart.isChecked() != aArticleOrSocialmedia.isCheckToggleButton())
                articleOrSocialmediaViewHolder.heart.setChecked(aArticleOrSocialmedia.isCheckToggleButton());

            articleOrSocialmediaViewHolder.setPosition(position);

            if (isArticle) {
                articleOrSocialmediaViewHolder.layoutBottomSocialMedia.setLayoutParams(paramForInvisibleView);
                articleOrSocialmediaViewHolder.articleDescriptionView.setLayoutParams(paramForVisibleViewWithWeight);
                articleOrSocialmediaViewHolder.socialMediaIconView.setLayoutParams(paramForInvisibleView);
            } else {
                articleOrSocialmediaViewHolder.layoutBottomSocialMedia.setLayoutParams(paramForVisibleView);
                articleOrSocialmediaViewHolder.articleDescriptionView.setLayoutParams(paramForInvisibleView);
                articleOrSocialmediaViewHolder.socialMediaIconView.setLayoutParams(paramForVisibleView);
            }

        } else if (articleOrSocialMediaList.get(position) instanceof DescriptionModel) {
            DescriptionModel descriptionModel = (DescriptionModel) articleOrSocialMediaList.get(position);
            DescriptionViewHolder descriptionViewHolder = (DescriptionViewHolder) holder;
            descriptionViewHolder.descriptionView.setText(descriptionModel.getDescription());
        } else if (articleOrSocialMediaList.get(position) instanceof NativeAdModel) {
            bindAdViewHolder(holder, articleOrSocialMediaList.get(position), activity);

        }
    }


    @Override
    public int getItemCount() {
        return articleOrSocialMediaList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (articleOrSocialMediaList.get(position) instanceof ArticleOrSocialmedia) {
            return TYPE_ARTICLE_OR_SOCIALMEDIA;
        } else if (articleOrSocialMediaList.get(position) instanceof DescriptionModel) {
            return TYPE_TEXT;
        } else if (articleOrSocialMediaList.get(position) instanceof NativeAdModel) {
            return TYPE_NATIVE_AD;
        }
        return 0;
    }

    private HolderEventListener createOnHolderEventListener() {
        return new HolderEventListener() {
            @Override
            public void onEditTextClickDone(int position) {
                ((ArticleOrSocialmedia) articleOrSocialMediaList.get(position)).incrementNumberOfComment();
                notifyItemChanged(position);
            }

            @Override
            public void onWhiteHeartClick(int position) {
                ((ArticleOrSocialmedia) articleOrSocialMediaList.get(position)).incrementNumberOfHearts();
                notifyItemChanged(position);
            }

            @Override
            public void onOrangeHeartClick(int position) {
                ((ArticleOrSocialmedia) articleOrSocialMediaList.get(position)).decrementNumberOfHearts();
                notifyItemChanged(position);
            }
        };
    }
}
