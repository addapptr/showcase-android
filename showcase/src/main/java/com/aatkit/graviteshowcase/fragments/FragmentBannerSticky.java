package com.aatkit.graviteshowcase.fragments;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.adapters.NewsAdapter;
import com.aatkit.graviteshowcase.models.DescriptionModel;
import com.aatkit.graviteshowcase.models.NewsModel;
import com.intentsoftware.addapptr.StickyBannerPlacement;

public class FragmentBannerSticky extends FragmentWithRecyclerView {

    private final int reloadInterval = 30;

    @Override
    void onCreate(LayoutInflater inflater, @Nullable ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_banner_sticky, container, false);

        Activity activity = getActivity();
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;

        if (activity != null) {
            activity.setTitle(R.string.banner);
            if (appCompatActivity.getSupportActionBar() != null)
                appCompatActivity.getSupportActionBar().setSubtitle(R.string.sticky_banner);
        }
    }

    @Override
    void createAdapter() {
        adapter = new NewsAdapter(list);
    }

    @Override
    public void onResume() {
        super.onResume();

        StickyBannerPlacement bannerPlacement = showcaseApplication.getStickyBannerPlacement();
        addPlacementView(bannerPlacement);
        bannerPlacement.startAutoReload();
    }

    @Override
    public void onPause() {
        StickyBannerPlacement bannerPlacement = showcaseApplication.getStickyBannerPlacement();

        bannerPlacement.stopAutoReload();
        removePlacementView(bannerPlacement);
        super.onPause();
    }

    private void addPlacementView(StickyBannerPlacement placement) {

        if (getActivity() != null) {
            FrameLayout mainLayout = (FrameLayout) getActivity().findViewById(R.id.banner_view);
            View placementView = placement.getPlacementView();

            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
            mainLayout.addView(placementView, layoutParams);
        }
    }

    private void removePlacementView(StickyBannerPlacement placement) {
        View placementView = placement.getPlacementView();

        if (placementView != null && placementView.getParent() != null) {
            ViewGroup parent = (ViewGroup) placementView.getParent();
            parent.removeView(placementView);
        }
    }

    @Override
    void prepareContentData() {


        String[] newsTitle = getResources().getStringArray(R.array.news_title);
        String[] news = getResources().getStringArray(R.array.news);
        String[] preface = getResources().getStringArray(R.array.prefaces);

        for (int j = 0; j < 3; j++) {
            for (int i = 0; i < 15; i++) {
                list.add(new NewsModel(newsTitle[i], news[i]));
            }
        }

        list.add(1, new DescriptionModel(preface[7]));
        list.add(3, new DescriptionModel(preface[8]));
        list.add(5, new DescriptionModel(String.format(preface[4], reloadInterval)));

        for (int i = 1; i < 5; i++) {
            list.add(i * 10 + 4, new DescriptionModel(String.format(preface[4], reloadInterval)));
        }
    }
}
