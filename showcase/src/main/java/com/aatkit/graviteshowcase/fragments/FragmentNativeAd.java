package com.aatkit.graviteshowcase.fragments;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.adapters.NativeAdViewAdapter;
import com.aatkit.graviteshowcase.models.ArticleOrSocialmedia;
import com.aatkit.graviteshowcase.models.DescriptionModel;
import com.aatkit.graviteshowcase.models.NativeAdModel;
import com.google.android.material.tabs.TabLayout;
import com.intentsoftware.addapptr.NativePlacementListener;
import com.intentsoftware.addapptr.Placement;

import java.util.Random;

public class FragmentNativeAd extends FragmentWithRecyclerView {


    @Override
    void onCreate(LayoutInflater inflater, @Nullable ViewGroup container) {

        rootView = inflater.inflate(R.layout.fragment_native_ad, container, false);

        Activity activity = getActivity();
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;

        if (activity != null) {
            activity.setTitle(R.string.native_ad);
            if (appCompatActivity.getSupportActionBar() != null)
                appCompatActivity.getSupportActionBar().setSubtitle(null);
        }

        TabLayout tabLayout = rootView.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText("Article"));
        tabLayout.addTab(tabLayout.newTab().setText("Social Media"));

        tabLayout.addOnTabSelectedListener(createOnTabSelectedListener());


    }

    @Override
    public void onResume() {
        super.onResume();
        showcaseApplication.getNativeAdPlacement().setListener(createNativePlacementListener());
    }

    @Override
    public void onDestroy() {
        showcaseApplication.getNativeAdPlacement().setListener(null);
        super.onDestroy();
    }

    @Override
    void createAdapter() {
        adapter = new NativeAdViewAdapter(list, getActivity());
    }

    private TabLayout.OnTabSelectedListener createOnTabSelectedListener() {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        ((NativeAdViewAdapter) adapter).showArticle(true);
                        adapter.notifyDataSetChanged();
                        break;
                    case 1:
                        ((NativeAdViewAdapter) adapter).showArticle(false);
                        adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    @Override
    void prepareContentData() {

        Random r = new Random();
        String[] newsTitle = getResources().getStringArray(R.array.news_title);
        String[] news = getResources().getStringArray(R.array.news);
        String[] preface = getResources().getStringArray(R.array.prefaces);

        for (int i = 0; i < 3; i++) {
            list.add(new ArticleOrSocialmedia(newsTitle[0], news[0], R.drawable.ic_image_0, R.mipmap.ic_avatar_1, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[1], news[1], R.drawable.ic_image_1, R.mipmap.ic_avatar_2, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[2], news[2], R.drawable.ic_image_2, R.mipmap.ic_avatar_3, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[3], news[3], R.drawable.ic_image_3, R.mipmap.ic_avatar_4, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[4], news[4], R.drawable.ic_image_4, R.mipmap.ic_avatar_5, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[5], news[5], R.drawable.ic_image_5, R.mipmap.ic_avatar_1, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[6], news[6], R.drawable.ic_image_6, R.mipmap.ic_avatar_2, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[7], news[7], R.drawable.ic_image_7, R.mipmap.ic_avatar_3, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[8], news[8], R.drawable.ic_image_8, R.mipmap.ic_avatar_4, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[9], news[9], R.drawable.ic_image_9, R.mipmap.ic_avatar_5, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[10], news[10], R.drawable.ic_image_10, R.mipmap.ic_avatar_1, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[11], news[11], R.drawable.ic_image_11, R.mipmap.ic_avatar_2, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[12], news[12], R.drawable.ic_image_12, R.mipmap.ic_avatar_3, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[13], news[13], R.drawable.ic_image_13, R.mipmap.ic_avatar_4, r.nextInt(110) + 10, r.nextInt(10) + 3));
            list.add(new ArticleOrSocialmedia(newsTitle[14], news[14], R.drawable.ic_image_14, R.mipmap.ic_avatar_4, r.nextInt(110) + 10, r.nextInt(10) + 3));
        }

        list.add(1, new DescriptionModel(preface[9]));
        list.add(3, new DescriptionModel(preface[10]));
        list.add(5, new DescriptionModel(preface[11]));

        for (int i = 0; i < 5; i++) {
            list.add(i * 10 + 6, new NativeAdModel(createOnAdLoadListener(), list.size(), showcaseApplication.getNativeAdPlacement()));
        }
    }

    NativePlacementListener createNativePlacementListener() {
        return new NativePlacementListener() {
            @Override
            public void onNoAd(@NonNull Placement placement) {

            }

            @Override
            public void onHaveAd(@NonNull Placement placement) {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i) instanceof NativeAdModel) {

                        if (!((NativeAdModel) list.get(i)).hasNativeAd()) {
                            ((NativeAdModel) list.get(i)).setNativeAdData(showcaseApplication.getNativeAdPlacement().getNativeAd());
                            break;
                        }
                    }
                }
            }

            @Override
            public void onResumeAfterAd(@NonNull Placement placement) {

            }

            @Override
            public void onPauseForAd(@NonNull Placement placement) {

            }
        };
    }
}
