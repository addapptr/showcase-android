package com.aatkit.graviteshowcase.fragments;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.adapters.NewsAdapter;
import com.aatkit.graviteshowcase.models.BannerCacheModel;
import com.aatkit.graviteshowcase.models.DescriptionModel;
import com.aatkit.graviteshowcase.models.NewsModel;

public class FragmentBannerCache extends FragmentWithRecyclerView {

    @Override
    void onCreate(LayoutInflater inflater, @Nullable ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_banner_with_recycler_view, container, false);

        Activity activity = getActivity();
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;

        if (activity != null) {
            activity.setTitle(R.string.banner);
            if (appCompatActivity.getSupportActionBar() != null)
                appCompatActivity.getSupportActionBar().setSubtitle(R.string.banner_cache);
        }
    }

    @Override
    void createAdapter() {
        adapter = new NewsAdapter(list);
    }

    @Override
    void prepareContentData() {

        String[] newsTitle = getResources().getStringArray(R.array.news_title);
        String[] news = getResources().getStringArray(R.array.news);
        String[] preface = getResources().getStringArray(R.array.prefaces);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 15; j++) {
                list.add(new NewsModel(newsTitle[j], news[j]));
            }
        }

        list.add(1, new DescriptionModel(preface[5]));
        list.add(3, new DescriptionModel(preface[1]));
        list.add(5, new DescriptionModel(preface[2]));

        for (int i = 0; i < 5; i++) {
            list.add(i * 10 + 7, new DescriptionModel(preface[6]));
            list.add(i * 10 + 7, new BannerCacheModel(showcaseApplication.getBannerCache()));
        }
    }
}
