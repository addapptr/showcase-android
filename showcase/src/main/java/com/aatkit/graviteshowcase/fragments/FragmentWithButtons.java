package com.aatkit.graviteshowcase.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.aatkit.graviteshowcase.R;
import com.intentsoftware.addapptr.AATKitReward;
import com.intentsoftware.addapptr.Placement;
import com.intentsoftware.addapptr.RewardedVideoPlacement;
import com.intentsoftware.addapptr.RewardedVideoPlacementListener;

public abstract class FragmentWithButtons extends Fragment {

    private Button buttonReload;
    private Button buttonShow;
    private ProgressBar progressBar;

    TextView loadStateTextView;

    View rootView;

    RewardedVideoPlacement adPlacement = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        onCreate(inflater, container);

        buttonReload = rootView.findViewById(R.id.button_reload);
        buttonReload.setOnClickListener(createOnClickReloadButtonListener());

        buttonShow = rootView.findViewById(R.id.button_show);
        buttonShow.setOnClickListener(createOnClickShowButtonListener());
        buttonShow.setEnabled(false);

        progressBar = rootView.findViewById(R.id.progress_bar);
        loadStateTextView = rootView.findViewById(R.id.tv_ad_load_state);

        return rootView;
    }

    @Override
    public void onPause() {

        buttonReload.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
        super.onPause();
    }

    @Override
    public void onDestroy() {

        buttonReload.setOnClickListener(null);
        buttonReload = null;

        buttonShow.setOnClickListener(null);
        buttonShow = null;

        progressBar = null;

        loadStateTextView = null;

        super.onDestroy();
    }

    private View.OnClickListener createOnClickReloadButtonListener() {
        return view -> {
            buttonReload.setEnabled(false);
            buttonShow.setEnabled(false);
            progressBar.setVisibility(View.VISIBLE);
            onClickReloadButton();

            adPlacement.reload();
        };
    }

    private View.OnClickListener createOnClickShowButtonListener() {
        return view -> {
            buttonReload.setEnabled(true);
            buttonShow.setEnabled(false);
            loadStateTextView.setVisibility(View.INVISIBLE);
            adPlacement.show();
        };
    }

    RewardedVideoPlacementListener createRewardedVideoPlacementListener() {
        return new RewardedVideoPlacementListener() {
            @Override
            public void onUserEarnedIncentive(@NonNull Placement placement, @Nullable AATKitReward aatKitReward) {
                FragmentWithButtons.this.onUserEarnedIncentive(aatKitReward);
            }

            @Override
            public void onNoAd(@NonNull Placement placement) {
                buttonReload.setEnabled(true);
                buttonShow.setEnabled(false);
                progressBar.setVisibility(View.INVISIBLE);
                loadStateTextView.setVisibility(View.VISIBLE);
                loadStateTextView.setText(R.string.ad_failed_message);
            }

            @Override
            public void onHaveAd(@NonNull Placement placement) {
                buttonReload.setEnabled(false);
                buttonShow.setEnabled(true);
                progressBar.setVisibility(View.INVISIBLE);
                loadStateTextView.setVisibility(View.VISIBLE);
                loadStateTextView.setText(R.string.ad_loaded_message);
            }

            @Override
            public void onResumeAfterAd(@NonNull Placement placement) {

            }

            @Override
            public void onPauseForAd(@NonNull Placement placement) {

            }
        };
    }

    abstract void onClickReloadButton();

    abstract void onCreate(LayoutInflater inflater, @Nullable ViewGroup container);

    abstract void onUserEarnedIncentive(AATKitReward aatKitReward);
}
