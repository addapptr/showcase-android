package com.aatkit.graviteshowcase.fragments;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aatkit.graviteshowcase.ShowcaseApplication;
import com.aatkit.graviteshowcase.R;
import com.intentsoftware.addapptr.AATKitReward;

import java.util.Locale;

public class FragmentRewardedVideo extends FragmentWithButtons {

    private TextView coinsEarnedMessage;
    private TextView coinsCounterTextView;

    private int coins = 0;

    private ShowcaseApplication showcaseApplication;
    private boolean shouldRunRewardRunnable;
    private Runnable onRewardEarnedRunnable;

    @Override
    void onCreate(LayoutInflater inflater, @Nullable ViewGroup container) {

        if (getActivity() != null)
            showcaseApplication = ((ShowcaseApplication) getActivity().getApplication());

        rootView = inflater.inflate(R.layout.fragment_rewarded_video, container, false);

        Activity activity = getActivity();
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;

        if (activity != null) {
            activity.setTitle(R.string.rewarded_video);
            if (appCompatActivity.getSupportActionBar() != null)
                appCompatActivity.getSupportActionBar().setSubtitle(null);
        }

        adPlacement = showcaseApplication.getRewardedVideoPlacement();

        coinsEarnedMessage = rootView.findViewById(R.id.tv_earned_coins_message);
        coinsCounterTextView = rootView.findViewById(R.id.tv_coins_counter);

        onRewardEarnedRunnable = () -> {
            coins += 50;
            loadStateTextView.setVisibility(View.INVISIBLE);
            coinsEarnedMessage.setVisibility(View.VISIBLE);
            coinsCounterTextView.setText(String.format(Locale.US, "%s%d", getString(R.string.coins_message), coins));
        };
    }

    @Override
    void onClickReloadButton() {
        if (coinsEarnedMessage != null)
            coinsEarnedMessage.setVisibility(View.INVISIBLE);
    }

    @Override
    void onUserEarnedIncentive(AATKitReward aatKitReward) {
        if (isAdded()) {
            shouldRunRewardRunnable = false;
            onRewardEarnedRunnable.run();
        } else {
            shouldRunRewardRunnable = true; //handle situations when reward callback is triggered when fragment is hidden
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (adPlacement != null)
            adPlacement.setListener(createRewardedVideoPlacementListener());

        if (shouldRunRewardRunnable) {
            shouldRunRewardRunnable = false;
            onRewardEarnedRunnable.run();
        }
    }

    @Override
    public void onPause() {

        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (adPlacement != null)
            adPlacement.setListener(null);
        coins = 0;
        showcaseApplication = null;
        onRewardEarnedRunnable = null;
        super.onDestroy();
    }
}
