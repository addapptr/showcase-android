package com.aatkit.graviteshowcase.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aatkit.graviteshowcase.AdReloadingRecyclerViewScrollListener;
import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.ShowcaseApplication;
import com.aatkit.graviteshowcase.listeners.AdLoadListener;
import com.aatkit.graviteshowcase.models.BannerCacheModel;
import com.aatkit.graviteshowcase.models.BannerModel;
import com.aatkit.graviteshowcase.models.InFeedBannerModel;
import com.aatkit.graviteshowcase.models.NativeAdModel;

import java.util.ArrayList;
import java.util.List;

abstract class FragmentWithRecyclerView extends Fragment {

    private SwipeRefreshLayout swipeContainer;
    private RecyclerView recyclerView;
    private AdReloadingRecyclerViewScrollListener scrollListener;

    View rootView;
    RecyclerView.Adapter adapter;
    ShowcaseApplication showcaseApplication;
    final List<Object> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (getActivity() != null)
            showcaseApplication = ((ShowcaseApplication) getActivity().getApplication());

        onCreate(inflater, container);

        recyclerView = rootView.findViewById(R.id.recycler_view);
        swipeContainer = rootView.findViewById(R.id.swipe_refresh_layout);
        prepareContentData();

        createAdapter();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(rootView.getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        if (getContext() != null) {
            recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        }
        scrollListener = new AdReloadingRecyclerViewScrollListener(list);
        recyclerView.addOnScrollListener(scrollListener);

        swipeContainer.setOnRefreshListener(createOnRefreshListener());
        swipeContainer.setColorSchemeColors(
                getResources().getColor(R.color.colorCheckedSelector),
                getResources().getColor(R.color.gradientEndColor),
                getResources().getColor(R.color.gradientStartColor));
        swipeContainer.setProgressViewOffset(true, 0, 60);

        return rootView;
    }

    @Override
    public void onPause() {
        scrollListener.onPause();

        for (Object model : list) {
            if (model instanceof NativeAdModel) {
                ((NativeAdModel) model).removeAd(); //remove all ads, they will be reloaded on activity resume
            } else if (model instanceof InFeedBannerModel) {
                ((InFeedBannerModel) model).removeAd(); //remove all ads, they will be reloaded on activity resume
            } else if (model instanceof BannerCacheModel){
                ((BannerCacheModel) model).removeAd();
            } else if (model instanceof BannerModel) {
                ((BannerModel) model).removeAd(); //remove all ads, they will be reloaded on activity resume
            }
        }
        recyclerView.scrollToPosition(0);

        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        scrollListener.onResume();
        scrollListener.ReloadVisibleAds(recyclerView);
    }

    @Override
    public void onDestroy() {
        swipeContainer = null;
        scrollListener = null;
        super.onDestroy();
    }

    AdLoadListener createOnAdLoadListener() {
        return new AdLoadListener() {
            @Override
            public synchronized void onRequestCompleted(int position) {
                adapter.notifyItemChanged(position);
            }
        };
    }

    private SwipeRefreshLayout.OnRefreshListener createOnRefreshListener() {
        return () -> {

            for(Object object: list){
                if (object instanceof BannerCacheModel){
                    ((BannerCacheModel)object).removeAd();
                }
            }

            list.clear();
            prepareContentData();
            adapter.notifyDataSetChanged();
            swipeContainer.setRefreshing(false);
        };
    }

    abstract void onCreate(LayoutInflater inflater, @Nullable ViewGroup container);

    abstract void createAdapter();

    abstract void prepareContentData();
}
