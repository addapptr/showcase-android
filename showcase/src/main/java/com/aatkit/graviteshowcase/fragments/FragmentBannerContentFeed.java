package com.aatkit.graviteshowcase.fragments;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aatkit.graviteshowcase.R;
import com.aatkit.graviteshowcase.adapters.NewsAdapter;
import com.aatkit.graviteshowcase.models.BannerModel;
import com.aatkit.graviteshowcase.models.DescriptionModel;
import com.aatkit.graviteshowcase.models.NewsModel;
import com.intentsoftware.addapptr.Placement;
import com.intentsoftware.addapptr.StickyBannerPlacementListener;

public class FragmentBannerContentFeed extends FragmentWithRecyclerView {

    private static final int reloadInterval = 30;

    @Override
    void onCreate(LayoutInflater inflater, @Nullable ViewGroup container) {
        rootView = inflater.inflate(R.layout.fragment_banner_with_recycler_view, container, false);

        Activity activity = getActivity();
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;

        if (activity != null) {
            activity.setTitle(R.string.banner);
            if (appCompatActivity.getSupportActionBar() != null)
                appCompatActivity.getSupportActionBar().setSubtitle(R.string.banner_in_content_feed);
        }

        showcaseApplication.getContentFeedBannerPlacement().setListener(createStickyBannerPlacementListener());

    }

    @Override
    void createAdapter() {
        adapter = new NewsAdapter(list);
    }

    @Override
    void prepareContentData() {

        String[] newsTitle = getResources().getStringArray(R.array.news_title);
        String[] news = getResources().getStringArray(R.array.news);
        String[] preface = getResources().getStringArray(R.array.prefaces);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 15; j++) {
                list.add(new NewsModel(newsTitle[j], news[j]));
            }
        }

        list.add(1, new DescriptionModel(preface[0]));
        list.add(3, new DescriptionModel(preface[1]));
        list.add(5, new DescriptionModel(preface[2]));

        for (int i = 0; i < 6; i++) {
            list.add(i * 10 + 7, new DescriptionModel(String.format(preface[4], reloadInterval)));
            list.add(i * 10 + 7, new BannerModel(createOnAdLoadListener(), list.size(), showcaseApplication.getContentFeedBannerPlacement()));
            list.add(i * 10 + 7, new DescriptionModel(preface[3]));
        }
    }

    StickyBannerPlacementListener createStickyBannerPlacementListener() {
        return new StickyBannerPlacementListener() {
            @Override
            public void onNoAd(@NonNull Placement placement) {

            }

            @Override
            public void onHaveAd(@NonNull Placement placement) {
                for (Object model : list) {
                    if (model instanceof BannerModel) {
                        BannerModel bannerModel = (BannerModel) model;
                        if (!bannerModel.hasBanner()) {
                            bannerModel.setBannerPlacementLayout(showcaseApplication.getContentFeedBannerPlacement().getPlacementView());
                        }
                    }
                }
            }

            @Override
            public void onResumeAfterAd(@NonNull Placement placement) {

            }

            @Override
            public void onPauseForAd(@NonNull Placement placement) {

            }
        };
    }
}
