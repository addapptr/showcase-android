package com.aatkit.graviteshowcase.fragments;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aatkit.graviteshowcase.R;

public class FragmentInterstitial extends Fragment {

    private static final String EXTRA_AD_SHOWN_FROM_START = "adShownFromStart";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_interstitial, container, false);

        Activity activity = getActivity();
        AppCompatActivity appCompatActivity = (AppCompatActivity) activity;

        if (activity != null) {
            activity.setTitle(R.string.interstitial);
            if (appCompatActivity.getSupportActionBar() != null)
                appCompatActivity.getSupportActionBar().setSubtitle(null);
        }

        TextView startupInterstitialTextView = rootView.findViewById(R.id.start_up_interstitial_text_view);

        if (getArguments() != null && (boolean) getArguments().getSerializable(EXTRA_AD_SHOWN_FROM_START)) {
            startupInterstitialTextView.setVisibility(View.VISIBLE);
        }

        return rootView;
    }
}
