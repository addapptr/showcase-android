// Copyright 2014 Google Inc. All Rights Reserved.

package com.aatkit.graviteshowcase;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

public class SampleVideoPlayer extends VideoView {

    private MediaController mediaController;
    private static MediaPlayer mediaPlayer;

    public interface OnVideoCompletedListener {

        void onVideoCompleted();
    }

    private List<OnVideoCompletedListener> onVideoCompletedListeners = new ArrayList<>(1);

    public SampleVideoPlayer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SampleVideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SampleVideoPlayer(Context context) {
        super(context);
        init();
    }

    private void init() {
        mediaController = new MediaController(getContext());
        mediaController.setAnchorView(this);

        super.setOnCompletionListener(mediaPlayer -> {
            SampleVideoPlayer.mediaPlayer = mediaPlayer;
            SampleVideoPlayer.mediaPlayer.reset();
            SampleVideoPlayer.mediaPlayer.setDisplay(getHolder());

            for (OnVideoCompletedListener listener : onVideoCompletedListeners) {
                listener.onVideoCompleted();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onDestroy() {

        if (mediaController != null) {
            mediaController.setAnchorView(null);
            mediaController.removeAllViews();
            mediaController.removeAllViewsInLayout();
            mediaController.hide();
            mediaController = null;
        }

        if (onVideoCompletedListeners != null) {
            onVideoCompletedListeners.clear();
            onVideoCompletedListeners = null;
        }

        setVideoURI(null);

        stopPlayback();
        cancelPendingInputEvents();
        setMediaController(null);
        setOnPreparedListener(null);
        setOnInfoListener(null);
        setOnClickListener(null);
        setOnCompletionListener(null);
    }

    public void addVideoCompletedListener(OnVideoCompletedListener listener) {
        onVideoCompletedListeners.add(listener);
    }
}
